//
//  HealthStoreHelper.swift
//  Mywellness
//
//  Created by Vincenzo Scafuto on 31/10/2017.
//  Copyright © 2017 Technogym S.p.A. All rights reserved.
//

import HealthKit

protocol HealthStoreAgent {
    static var sharedTypes: Set<HKSampleType> { get }
    static var readTypes: Set<HKObjectType> { get }
}

class HealthStoreHelper {
    
    private enum HealthStoreError: Error {
        case notAvailableOnDevice
        case dataTypeNotAvailable
    }
    
    static func authorize(toWrite writeSet: Set<HKSampleType>, read readSet: Set<HKObjectType>,
                          completion: @escaping(Bool, Error?) -> Void)
    {
        guard HKHealthStore.isHealthDataAvailable() else {
            completion(false, HealthStoreError.notAvailableOnDevice)
            return
        }
        HKHealthStore().requestAuthorization(toShare: writeSet, read: readSet, completion: completion)
    }

    static func authorize(agents: [HealthStoreAgent.Type], completion: @escaping(Bool, Error?) -> Void) {
        let readSet = Set(agents.flatMap({ $0.readTypes }))
        let shareSet = Set(agents.flatMap({ $0.sharedTypes }))
        authorize(toWrite: shareSet, read: readSet, completion: completion)
    }

    static func shouldAskPermission(for collection: [HKObjectType]) -> Bool {
        guard HKHealthStore.isHealthDataAvailable() else {
            return false
        }

        return collection.map(HKHealthStore.shared.authorizationStatus)
            .contains(.notDetermined)
    }
}

extension HKHealthStore {
    static let shared = HKHealthStore()
}
