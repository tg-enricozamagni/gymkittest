//
//  ViewController.swift
//  GymKitTest
//
//  Created by Enrico Zamagni on 13/11/17.
//  Copyright © 2017 Technogym. All rights reserved.
//

import UIKit
import HealthKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        HealthStoreHelper.authorize(agents: [ ViewController.self ]) { (success, error) in
            guard success else {
                return
            }

            DispatchQueue.main.async {
                self.fetchData()
            }
        }
    }

    private func fetchData() {
        let calendar = Calendar.current
        var fetchFromComponents = DateComponents()
        fetchFromComponents.day = 8
        fetchFromComponents.month = 11
        fetchFromComponents.year = 2017
        let startDate = calendar.date(from: fetchFromComponents)

        let dateSampleSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: true)

        HKHealthStore.shared.execute(HKSampleQuery(
            sampleType: HKObjectType.workoutType(),
            predicate: HKQuery.predicateForSamples(withStart: startDate, end: Date(), options: []),
            limit: HKObjectQueryNoLimit,
            sortDescriptors: [ dateSampleSortDescriptor ],
            resultsHandler: { (query, samples, error) in
                guard let samples = samples else {
                    return
                }

                let workouts = samples
                    .filter({ $0.device?.localIdentifier?.contains("fitnessmachine") == true })
                    .flatMap({ $0 as? HKWorkout })

                for workout in workouts {
                    HKHealthStore.shared.execute(HKSampleQuery(
                        sampleType: HKObjectType.quantityType(forIdentifier: .heartRate)!,
                        predicate: HKQuery.predicateForSamples(withStart: workout.startDate, end: workout.endDate, options: [ .strictStartDate, .strictEndDate ]),
                        limit: HKObjectQueryNoLimit,
                        sortDescriptors: [ dateSampleSortDescriptor ],
                        resultsHandler: { (_, samples, error) in
//                            print(samples)
                    }))

                    HKHealthStore.shared.execute(HKSampleQuery(
                        sampleType: HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)!,
                        predicate: HKQuery.predicateForObjects(from: workout),
                        limit: HKObjectQueryNoLimit,
                        sortDescriptors: [ dateSampleSortDescriptor ],
                        resultsHandler: { (_, samples, error) in
//                            print(samples)
                    }))

                    var components = DateComponents()
                    components.second = 5

                    let query = HKStatisticsCollectionQuery(
                        quantityType: HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)!,
                        quantitySamplePredicate: HKQuery.predicateForObjects(from: workout),
                        options:.cumulativeSum,
                        anchorDate: workout.startDate,
                        intervalComponents: components)
                    query.initialResultsHandler = { (query, collection, error) in
                        print(collection)
                    }

                    HKHealthStore.shared.execute(query)
                }
        }))
    }

}

extension ViewController: HealthStoreAgent {

    static var sharedTypes: Set<HKSampleType> {
        return []
    }

    static var readTypes: Set<HKObjectType> {
        return [
            HKObjectType.workoutType(),
            HKObjectType.quantityType(forIdentifier: .heartRate)!,
            HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)!
        ]
    }

}
